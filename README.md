# realMethods Application Generator For GitLab

![alt text](http://www.realmethods.com/img/gitlab_realmethods_.png)

This realMethods GitLab config file is a simple, yet powerful way for you to leverage GitLab to automate the generation of an MVP-quality application that can be built on, tested, and deployed through your GitLab CI/CD pipeline back onto GitLab.

Take the following easy steps to using this config file to generate your application.

#### Step 1 - Get this project

Clone this project in one of two ways:

*SSH*
_git@gitlab.com:realmethods-public/gitlabappgen.git_

*HTTPS*
_https://gitlab.com/realmethods-public/gitlabappgen.git_

#### Step 2 - Make changes

##### API_TOKEN

The api_token is required to initialize a unique session with the realMethods backend.  The one provided as default is safe to 
use fo any GitLab user. However, if you prefer to use your own, click [here](http://www.realmethods.com/developer.html) to create an account and access a unique API token.

##### Application Generation Configuration Examples 

To invoke application generation, one mandatory file is required, along with three optional files the mandatory file can reference.  If any of the option files is included as an input argument, the related internal reference of the mandatory is ignored. __These files are referenced from the root of your Git repository since they are being accessed from the .gitlab-ci.yml file__.

Visit [https://github.com/realmethods-public/orb](https://gitlab.com/realmethods-public/gitlabappgen) to view all sample model and config files.

[Click here](https://www.realmethods.com/cli.html#config-files) to learn more about configuration files and settings.


####### GENERATE\_YAML\_FILE (mandatory):
  
This YAML file contains the directives required to generate an application using a model identifier (by id or file_path), technology stack (by id or name), application options file, Git repo params or more.  
  
* Learn more [here](http://www.realmethods.com/cli.html#applicationgenerationconfigurationparameters). 
* See an example [here](https://gitlab.com/realmethods-public/gitlabappgen/blob/master/samples/yamls/generate-django.yml)
  
####### GIT\_PARAMS\_FILE (optional):
    
This optional YAML file contains one or more groupings of parameters to control committing an application's files (language   specific source code, build files, config files, CI/CD files, etc..) to a Git repository. If this argument is not provided, the _gitParams-->file_ param of the _generation-yaml-file_ is used.  
  
* Learn more [here](http://www.realmethods.com/cli.html#http://www.realmethods.com/cli.html#githubconfigurationparameters).
* See an example [here](https://gitlab.com/realmethods-public/gitlabappgen/blob/master/samples/git/gitlab.yml)

####### APP\_OPTIONS\_FILE (optional):  
  
This optional JSON file contains one or more groupings of parameters to control application generation flow and actual output. This file is where you would provide such things as database access params, application params (name, description, etc..), and so forth. If this argument is not provided the _appOptionsFile_ param of the _generation-yaml-file_ is used.  
  
* Learn more [here](http://www.realmethods.com/cli.html#http://www.realmethods.com/cli.html#appconfigurationparameters).
* See an example [here](https://github.com/realmethods-public/orb/blob/master/samples/options/Django.options.json)

####### MODEL_FILE (optional):  
A model identifier can be a model file ([see supported models](http://www.realmethods.com/api.html#supportedmodels)) or the realMethods identifier of a previously used/published model. If this argument is not provided the _modelId_ param of the  _generation-yaml-file_ is used.  
  
* Learn more [here](http://www.realmethods.com/cli.html#applicationgenerationconfigurationparameters).
* Learn about [supported models](http://www.realmethods.com/models.html).
* See an example [here](https://gitlab.com/realmethods-public/gitlabappgen/blob/master/samples/models/reference_management.xmi)


####### AWS Credentials

Note: If using one of the AWS Lambda tech stacks, you will have to assign the access key and secret key as project level environment variables.  See https://gitlab.com/help/ci/variables/README#variables for more details. Be sure to name the accesskey USER\_AWS\_ACCESSKEY and name the secretkey USER\_AWS\_SECRETKEY.  Equally important, 
make sure you have the correct policies assigned for the related user (_AWSCodeDeployRoleForLambda, AWSLambdaExecute, AWSLambdaRole_, etc..)


#### Step 3 - Create a 2 Project Repos

First create a project repository for the generated application files.  This repo name is the name you assigned in the gitlab.yml sample file.

Next, create a project repository within GitLab for this project.  Any name will do.


#### Step 4 - Commit This Project

Upon committing this project to GitLab, the _.gitlab-ci.yml_ your modified will be read and executed.

#### Step 5 - Observe Generated App Files

During executing of Step4, as a final step, realMethods will commit the generated app files to the repository you assigned as the 1st repository in Step 3.

#### Step 5 - Observe Pipeline Execution

Upon commitment of both projects in Step 3, GitLab will automatically execute the _.gitlab-ci.yml_ file.  For the first repo, you should see your generated application being built and tested.

#### Step 5 - Congratulations!

You just generated an entire application complete with core capabilities, build file, CI/CD config, and more....

Best of luck finishing the app!


